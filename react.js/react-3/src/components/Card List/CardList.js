import React, {Component} from 'react';
import './CardList.scss'
import Card from "../Card/Card";
import Modal from "../Modal/Modal";
import Header from "../Header/Header";
import Cart from '../Cart/Cart'
import Favorites from "../Favorites/Favorites";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

class CardList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allCards: props.cards,
            modal: false,
            clickedBtnID: null,
            AddedCards: []
        }

    }


    //Function for showing modal window
    modalShow = (e) => {
        this.setState({
            modal: true,
            clickedBtnID: e.target.id
        });
        // console.log(e.target);
        document.querySelector('.App').classList.add('active');
    };


    //Function for closing modal window
    closeButton = () => {
        this.setState({
            modal: false
        });
        document.querySelector('button').classList.remove('active');
        document.querySelector('.App').classList.remove('active');
    };


    //Function for adding the card to the local storage
    AddToCard = () => {
        let card = this.state.allCards[this.state.clickedBtnID];
        this.state.AddedCards.push(card);
        localStorage.setItem('cards', JSON.stringify(this.state.AddedCards));
        this.closeButton();
    };


    render() {
        const cards = this.state.allCards;
        const CardListPage = () => {
            return (
                <div className="card-list">
                    {this.state.modal === false
                        ?
                        cards.map((card) => {
                            return <Card
                                cards={cards}
                                key={card.number}
                                path={card.path}
                                name={card.name}
                                text={card.text}
                                price={card.price}
                                color={card.color}
                                id={card.number}
                                onclick={this.modalShow}/>

                        }) : null
                    }
                    {this.state.modal ?
                        <Modal closeModalButton={this.closeButton} AddToCard={this.AddToCard}/>
                        : null
                    }

                </div>
            )
        };

        return <div className='card-list-content'>
            <Router>
                {this.state.modal === false
                    ?
                    <Header/>
                    : null
                }
                <Switch>
                    <Route path={'/'} exact component={CardListPage}/>
                    <Route path={'/cart'} component={Cart}/>
                    <Route path={'/favorites'} component={Favorites}/>
                </Switch>
            </Router>
        </div>
    }
}

export default CardList;