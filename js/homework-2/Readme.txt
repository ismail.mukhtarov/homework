------------1. Describe in your own words what the functions in programming are for.---------------
-----2. Describe in your own words what is the reason why arguments are defined in a function.-----
-----------------------Why are they passed when a function is called?------------------------------ 

---A JavaScript function is a block of code designed to perform a particular task. Functions provide 
better modularity for your application and a high degree of code reusing. In JavaScript, a function
allows you to define a block of code, give it a name and then execute it as many times as you want. 
You can use the same code many times with different arguments, to produce different results.

---A function definition (also called a function declaration, or function statement) consists of the 
function keyword, followed by:
# The name of the function.
# A list of function parameters, enclosed in brackets and separated by commas.
# The JavaScript statements that define the function, enclosed in curly brackets, {...}.
	
	                 	_______________________________________________________
			        | function name(parameter1, parameter2, parameter3) { |
 			        |	// code to be executed 			      |	
		         	| }						      |
		        	|_____________________________________________________|


---A JavaScript function is executed when "something" calls it.The code inside the function will 
execute when "something" invokes (calls) the function:

# When an event occurs (when a user clicks a button)
# When it is invoked (called) from JavaScript code
# Automatically (self invoked)

---Function Return
When JavaScript reaches a return statement, the function will stop executing. If the function was 
invoked from a statement, JavaScript will "return" to execute the code after the invoking statement.

---Example
Let's write some example with function. Let's create the function with name sum, with parameter "a" 
and "b", which will return the sum of the a and b:

	                 	_______________________________________________________
			        | 	function sum (a,b) { 			      |
 			        |		return a+b; 			      |	
		         	|	 }					      |
		        	|_____________________________________________________|

When we will call the function, and give the arguments to the "a" and "b" (for example,  4 and 5),
we will get the sum of the "a" and "b" i.e 9.


	                 	_______________________________________________________
			        | 	function sum (a,b) { 			      |
 			        |		return a+b; 			      |	
		         	|	 }					      |
				|	 console.log(sum(4,5));  // result will be 9  |
		        	|_____________________________________________________|

---------------------------------------------Thanks for attention :)------------------------------------



