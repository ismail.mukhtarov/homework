import React, {Component} from 'react';
import './Header.scss'
import HeaderCard from "./HeaderCard";
import addCard from "../img/add-card.png"

class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            card: false
        };
    }

    showCard = () => {
        this.setState({
            card: !this.state.card
        });
    };

    render() {
        let AddedCards =  JSON.parse(localStorage.getItem('cards'))
        return (
            <div className='header'>

                <a className='header-links' href="#">HOME</a>
                <a className='header-links' href="#">ABOUT US</a>
                <a className='header-links' href="#">CONTACTS</a>

                <div className='header-link-card'>
                    <a onClick={this.showCard} className='header-links' href="#">
                        <img src={addCard} alt="add-card"/>
                    </a>
                    {this.state.card ?
                        <div className='header-card-list'>

                            {AddedCards === null ?
                                <h2>There is nothing here</h2>
                                : AddedCards.map((card, ind) => {
                                    return <HeaderCard key={ind} card={card}/>
                                })
                            }

                        </div>
                        : null
                    }
                </div>

            </div>
        );
    }
}

export default Header;