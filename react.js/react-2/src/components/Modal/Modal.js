import React, {Component} from 'react';
import './Modal.scss'
import icon from  '../img/x.png'

class Modal extends Component {
    constructor(props) {
        super(props);
        this.state={
            closeModalButton: props.closeModalButton,
            AddToCard: props.AddToCard
        };
    }

    render() {
        return (
                <div id='modal' className='modal'>
                    <div className='modal-header'>
                        Do you wand add it to cart?
                        <img  className='modal-icon' onClick={this.state.closeModalButton} src={icon} alt="icon"/>
                    </div>
                    <div className="modal-body">
                        <p className='modal-text'>Once you add this file, it won’t be possible to undo this action.
                            Are you sure you want to add it?</p>
                        <div className='modal-actions'>
                            <button className='modal-actions-buttons' onClick={this.state.AddToCard}>Yes</button>
                            <button className='modal-actions-buttons' onClick={this.state.closeModalButton} >No</button>
                        </div>
                    </div>
            </div>


        );
    }
}

export default Modal;