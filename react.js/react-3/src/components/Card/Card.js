import React, {Component} from 'react';
import './Card.scss'
import Button from "../Button/Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faStar} from '@fortawesome/free-solid-svg-icons'


class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cards: props.cards,
            path: props.path,
            name: props.name,
            text: props.text,
            price: props.price,
            color: props.color,
            id: props.id,
            onclick: props.onclick,
            starColor: true,
            favorites: []
        }

    }

    changeStar = (e) => {
        let favorites = [];
        let starIcon = e.target.parentElement;
        let card = this.state.cards[starIcon.parentElement.id];

        this.setState({
            starColor: !this.state.starColor
        });


        if (this.state.starColor) {
            e.target.style.color = 'gold';
            favorites.push(card);
            localStorage.setItem(`favorites${starIcon.parentElement.id}`, JSON.stringify(favorites));
        } else {
            e.target.style.color = 'black';
            // let card = this.state.cards[starIcon.parentElement.id];
            // localStorage.removeItem('favorites')
        }
    };


    render() {
        return (
            <div key={this.state.id} id={this.state.id} className="card">
                <img className='card-img' src={this.state.path} alt="image-1"/>
                <div className='card-info' id={this.state.id}>
                    <h5 className='card-name'>{this.state.name}</h5>
                    <FontAwesomeIcon className='star-icon' onClick={this.changeStar} icon={faStar}/>
                    <p className='card-text'>{this.state.text}</p>
                    <p className='card-price'>{this.state.price}</p>
                    <Button backgroundColor={this.state.color} id={this.state.id} text='ADD TO CART'
                            onclick={this.state.onclick}/>
                </div>
            </div>
        );
    }
}

export default Card;