import React, {Component} from 'react';
import './Button.scss'

class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {
            backgroundColor : this.props.backgroundColor,
            text: this.props.text,
            onclick: this.props.onclick
        }
    }

    render() {
        return (
                <button className='button' style={{backgroundColor: this.state.backgroundColor }} onClick={this.state.onclick} >{this.state.text}</button>
        );
    }
}

export default Button;