import React, {Component} from 'react';
import './Button.scss'

class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {
            backgroundColor : this.props.backgroundColor,
            text: this.props.text,
            onclick: this.props.onclick,
            id: this.props.id
        }
    }

    render() {
        return (
                <button id={this.state.id} className='button' style={{backgroundColor: this.state.backgroundColor }}  onClick={this.state.onclick} >{this.state.text}</button>
        );
    }
}

export default Button;