import React, {Component} from 'react';
import './App.scss'
import CardList from "./components/Card List/CardList";
import Header from "./components/Header/Header";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cards: []
        }
    }

    componentDidMount() {
        fetch('../../../products.json')
            .then(res => res.json())
            .then((data) => {
                this.setState({
                    cards: data
                });
                // console.log(this.state.cards);
            })
    }


    render() {
        return (
            <div className='App'>
                    {
                        this.state.cards.length > 0 ?
                            <CardList cards={this.state.cards}/>
                            : null
                    }
            </div>
        );
    }
}

export default App;