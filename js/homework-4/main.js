

function filterBy(array, type) {
    let newArr = [];

    for (let i of array){
        let a = typeof(i);
        if( a !== type ){
            newArr.push(i);
        }
    }
    return newArr;
}

let arr = ['hello', true, function (){} , 'world', false,23,'23', null ];

console.log(filterBy(arr, 'number'));
filterBy(arr, 'string');
filterBy(arr, 'object');
filterBy(arr, 'boolean');
filterBy(arr, 'function');








