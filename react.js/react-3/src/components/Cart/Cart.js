import React, {Component} from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faStar} from "@fortawesome/free-solid-svg-icons";
import Button from "../Button/Button";
import './Cart.scss'

class Cart extends Component {
    state = {
        starColor: true,
        favorites: [],
        localStorageCards: []
    };

    // Star color change
    changeStar = (e) => {
        this.setState({
            starColor: !this.state.starColor
        });
        if (this.state.starColor) {
            localStorage.setItem('favorites', JSON.stringify(this.state.favorites) );
            e.target.style.color = 'gold'
        } else {
            e.target.style.color = 'black'
        }
    };

    deleteCart = (e) => {
        console.log(e.target.id);
        this.setState({
            localStorageCards: localStorage.getItem('cards')
        });
        localStorage.removeItem('cards');

        localStorage.setItem('cards', this.state.localStorageCards);

    };



    render() {
        let AddedCards = JSON.parse(localStorage.getItem('cards'));
        // console.log(AddedCards);
        return (
            <div className='addedCards'>
                {AddedCards === null ?
                    <h1>There is nothing here !</h1>
                    :
                    AddedCards.map((AddedCard, ind) => {
                        return (
                            <div key={ind} id={AddedCard.number} className="card">
                                <img className='card-img' src={AddedCard.path} alt="image-1"/>
                                <div className='card-info'>
                                    <h5 className='card-name'>{AddedCard.name}</h5>
                                    <FontAwesomeIcon className='star-icon' onClick={this.changeStar} icon={faStar}/>
                                    <p className='card-text'>{AddedCard.text}</p>
                                    <p className='card-price'>{AddedCard.price}</p>
                                    <Button onclick={this.deleteCart} backgroundColor={AddedCard.color}
                                            id={AddedCard.number}
                                            text='DELETE CART'/>
                                </div>
                            </div>

                        )
                    })}
            </div>);
    }
}

export default Cart;