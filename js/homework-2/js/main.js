let num_1 = parseInt(prompt("Enter the first number"));   //first number
while (isNaN(num_1)){
    num_1 = parseInt(prompt("Please, enter the first number correctly"));
}
let num_2 = parseInt(prompt("Enter the second number"));   // second number
while (isNaN(num_2)){
    num_2 = parseInt(prompt("Please, enter the second number correctly"));
}
const operation = prompt("Enter the operation"); //operation

let result;
function calculation(number1,number2,calcOperation) {
    switch (operation) {
        case "+":
            result = number1 + number2;
            break;
        case "-":
            result = number1 - number2;
            break;
        case "*":
            result = number1 * number2;
            break;
        case "/":
            result = number1 / number2;
            break;
    }
    console.log(result);
}

calculation(num_1, num_2, operation);

