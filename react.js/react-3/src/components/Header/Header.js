import React, {Component} from 'react';
import './Header.scss'
import HeaderCard from "./HeaderCard";
import addCard from "../img/add-card.png"
import {BrowserRouter as Router, Link} from "react-router-dom";

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            card: false
        };
    }

    showCard = () => {
        this.setState({
            card: !this.state.card
        });
    };

    render() {
        let AddedCards = JSON.parse(localStorage.getItem('cards'))
        return (
            <div className='header'>
                <Link to={'/'} className='header-links'>HOME</Link>
                <Link to={'/cart'} className='header-links'>CART</Link>
                <Link to={'/favorites'} className='header-links'>FAVORITES</Link>
                    
                <div className='header-link-card'>
                    <a onClick={this.showCard} className='header-links' href="#">
                        <img src={addCard} alt="add-card"/>
                    </a>
                    {this.state.card ?
                        <div className='header-card-list'>

                            {AddedCards === null ?
                                <h2>There is nothing here</h2>
                                : AddedCards.map((card, ind) => {
                                    return <HeaderCard key={ind} card={card}/>
                                })
                            }

                        </div>
                        : null
                    }
                </div>
            </div>
        );
    }
}

export default Header;