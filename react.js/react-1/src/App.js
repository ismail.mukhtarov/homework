import React, {Component} from 'react';
import './App.scss'
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            FirstModalWindow: false,
            SecondModalWindow: false,
            ActivateButtons: true,
        }

    }


    showFirstModal = () => {
        this.setState({
            FirstModalWindow: true,
            ActivateButtons:false
        });
        document.querySelector('.bg-color').classList.add('active');
    };

    showSecondModal = () => {
        this.setState({
            SecondModalWindow: true,
            ActivateButtons:false
        });
        document.querySelector('.bg-color').classList.add('active');
    };

    closeButton = () => {
        this.setState({
            FirstModalWindow: false,
            SecondModalWindow: false,
            ActivateButtons: true
        });
        document.querySelector('.bg-color').classList.remove('active');
    };

    removeModal = (e) =>{
        if(e.target.classList.contains('active')){
            this.setState({
                        FirstModalWindow: false,
                        SecondModalWindow: false,
                        ActivateButtons: true
                    });
            document.querySelector('.bg-color').classList.remove('active');
        }
    };


    render() {
        return (
            <div onClick={this.removeModal} className='bg-color'>
                <div  className='App'>
                    {this.state.ActivateButtons ?
                        <Button key={1} backgroundColor='teal' text='Open first modal' onclick={this.showFirstModal}/>
                        : null
                    }
                    {this.state.ActivateButtons ?
                        <Button key={2} backgroundColor='green' text='Open second modal' onclick={this.showSecondModal}/>
                        : null
                    }
                    {this.state.FirstModalWindow ?
                        <Modal key={3} header='Do you want to delete this file?' text='Once you delete this file, it won’t be possible to undo this action.
                        Are you sure you want to delete it?' closeButton={this.closeButton} actions={[<Button backgroundColor='#b3382c' text='Ok'/>,
                            <Button backgroundColor='#b3382c' text='Cansel'/>]}/>
                        : null
                    }
                    {this.state.SecondModalWindow?
                        <Modal key={4} header='Do you want to delete React from your life?' text='Once you delete react, it won’t be possible to undo this action.
                        Are you sure you want to delete it?' closeButton={this.closeButton} actions={[<Button backgroundColor='#b3382c' key="1" text='Ok'/> , <Button key="2" backgroundColor='#b3382c' text='Cansel' />]}/>
                        : null
                    }
                </div>
            </div>
        );
    }
}

export default App;