--------------------------------1. Explain in your own words how the forEach loop works.----------------------------------

The forEach() method executes a provided function once for each array element.
It means, if we will write some function insine forEach loop, it will executed for each element of the array.
For example:

----------------------------const array1 = ['a', 'b', 'c'];-------------------------

----------------------------array1.forEach(element => console.log(element));-----------------

----------------------------// expected output: "a"-----------------------
----------------------------// expected output: "b"--------------------
----------------------------// expected output: "c"-----------------------

			It means , each array element will shown in console. 

And with this rule we can easly make some operation, under the each item of the array.

 -----------------------------------------------------Thanks for attention --------------------------------------------

