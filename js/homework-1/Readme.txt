------1. Explain in your own words the difference between declaring variables via `var`, `let` and `const`.------

At first let's say what is the variable. A variable is a �named storage� for data. For creating a variable , 
we use the keywords such as "var", "let" and "const". The main difference between variables is the scope. 
---Scope---
Scope means where the variables available for use. Var declarations are globally scoped or function scoped 
while let and const are block scoped.
---Globally or function(localy) scoped---
The var declaration is a globally scoped if the variable is declared in outside of function. This means that the 
any variable which declared with var outside a function block is avaliable for use in the whole window. But var
declaration is a function(local) scoped if the variable is declared inside the function.  This means that it is 
available and can be accessed only within that function.
---Block scoped---
A block is piece of code bounded by curly braces {}.  This could be a function, a for or while loop or an if statement.
So a variable declared in a block with the let, available only for use this variable inside the block. 

Also the var variables can be updated and re-declared inside its scope. Let variables can be updated but not 
re-daclared inside its scope, but const variables can neither be updated nor re-declared. It means that, for example
if we will re-declare the var variable,

						---var something = "Hello";---
						---var something = "Hello World";--- 

it will work, and will not give the error message. But if we wil write the same thing with let and const, it will give 
the error message.
If we will declare some let or var variable and try to update it, it will work and will not give the error message . 
But with const it will give the error message, because we can't change the  or re-declare const variables. For example,
if we will write:

			---var something = "Hello";---		or	---let something = "Hello";---
			---something = "Hello World";--- 		---something = "Hello World";--- 

it will work , and will nor give the error message. But if we will write the same thing with const, it will give the 
error message.
---Hoisting---
There are also the difference with hoisting. In JavaScript, a variable can be declared after using it. It means, for 
example, if we will write,
						---console.log (something);---
    						---var something = "Hello";---

it will interpreted like this:
					  	---var something;---
    					        ---console.log (something);--- 
   					        ---something = "Hello";---

It will give the result "undefined". It means that the, var variables are hoisted to the top of its scope and initialized
with a value "undefined"(and get the value "undefined").
The const and let declaration are also hoisted to the top of its scope, but are not initialized and we will get only the 
error message.

------2. Why is declaration of a variable via `var` considered a bad tone?------
May be the first disadvantige of the var variable is, if we will declare twice same variable, it will not give the error, 
but vice versa , if we will declare the same variable twice with const and let, we will get the error message.
And also we can say, for example, if we write the var variable inside the if statement or for and while loop, and call
variable with the same name in the other part of your code, then we will can get value of this variable and it can be 
the problem for us. The one simple example:

					     ---  var c = 8;        
					     ---  if (condition){ 
					     ---     //Do something
					     ---     var c = 9;  
					     ---  }    
						
					     ---  console.log(c);

The result of this code will be "9" and it can be the problem for us.

--------------------------------------------------Thanks for attention :) --------------------------------------------------

		



