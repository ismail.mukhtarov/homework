import React, {Component} from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faStar} from "@fortawesome/free-solid-svg-icons";
import Button from "../Button/Button";

// import './Cart.scss'

class Favorites extends Component {
    state = {
        starColor: true,
        favorites: [],
    };

    // Star color change
    changeStar = (e) => {
        this.setState({
            starColor: !this.state.starColor
        });
        if (this.state.starColor) {
            localStorage.setItem('favorites', JSON.stringify(this.state.favorites));
            e.target.style.color = 'gold'
        } else {
            e.target.style.color = 'black'
        }
    };


    render() {
        for (let i = 0; i < 20; i += 0) {
            let AddedCard = localStorage.getItem(`favorites${i}`);
            console.log(AddedCard);
            return (
                <div className='addedCards'>
                    {AddedCard === null ?
                        <h1>There is nothing here !</h1>
                        :
                        <div key={AddedCard[0].number} id={AddedCard[0].number} className="card">
                            <img className='card-img' src={AddedCard[0].path} alt="image-1"/>
                            <div className='card-info'>
                                <h5 className='card-name'>{AddedCard[0].name}</h5>
                                <FontAwesomeIcon className='star-icon' onClick={this.changeStar} icon={faStar}/>
                                <p className='card-text'>{AddedCard[0].text}</p>
                                <p className='card-price'>{AddedCard[0].price}</p>
                                <Button backgroundColor={AddedCard[0].color}
                                        id={AddedCard[0].number}
                                        text='DELETE CART'/>
                            </div>
                        </div>

                        // )
                        }
                        )
                    }
                </div>);
        }
    }
}

export default Favorites;