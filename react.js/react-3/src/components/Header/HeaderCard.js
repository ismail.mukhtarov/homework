import React, {Component} from 'react';
import './HeaderCard.scss'

class HeaderCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            card: props.card
        }
    }

    render() {
        // console.log(this.state.cards);
        return (
            <div className='header-card' >
                <img className='header-card-img' src={this.state.card.path} alt="image"/>
                <h3>{this.state.card.name}</h3>
            </div>
        );
    }
}

export default HeaderCard;